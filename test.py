import os
import time
import logging
from dotenv import load_dotenv
import pandas as pd
import pyautogui
from datetime import datetime
import xlwings as xw
load_dotenv(override=True)
logging.basicConfig(level=logging.INFO, filename='./logging/autorefresh.log',format='%(asctime)s :: %(levelname)s :: %(message)s')


path = os.environ.get("REFRESH_EXCEL_PATH")


#os.system(f"start /max EXCEL.EXE {path}") #Open full size
#time.sleep(3)
#pyautogui.hotkey("alt","f4")


try:
    book = xw.Book(path)
    book.save()
    book.close()
except Exception as e:
    print("No opened Exccel")