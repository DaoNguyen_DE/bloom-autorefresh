from pywinauto import Application
import os

path = os.environ.get("REFRESH_EXCEL_PATH")
os.system(f"start EXCEL.EXE {path}")
# Path to your Excel file
#excel_file_path = os.environ.get("REFRESH_EXCEL_PATH")

# Connect to the running Excel application or start it if not running
try:
    app = Application(backend="uia").connect(title_re='.*Excel.*')
except Exception:
    app = Application(backend="uia").start("excel.exe")

# Connect to the Excel window
window = app.window(title_re='.*Excel.*')

# Open the Excel file
window.wait('ready')



# Navigate to the desired cell
#window.wait('ready').type_keys("A1")  # Replace 'A1' with the desired cell
#window.type_keys("{ENTER}")

# Close the Excel application (optional)
# window.close()

