from pywinauto import Application
import os

excel_file_path = os.environ.get("REFRESH_EXCEL_PATH")

app = Application(backend="uia").start(r"C:/Program Files/Microsoft Office/Office16/EXCEL.EXE")

# Connect to the Excel window
window = app.window(title_re='.*Excel.*')

# Open the Excel file
window.wait('ready').type_keys("^o")  # Press Ctrl+O to open file
file_dialog = app.window(title='Open')
file_dialog.wait('ready').Edit.type_keys(excel_file_path)
file_dialog.Open.click()

# Navigate to the desired cell
window.wait('ready').type_keys("A1")  # Replace 'A1' with the desired cell
window.type_keys("{ENTER}")

# Close the Excel application (optional)
# window.close()