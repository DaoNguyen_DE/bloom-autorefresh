import pandas as pd
import gspread
from google.oauth2.service_account import Credentials
import test_openxl
import os
import time
path = os.environ.get("REFRESH_EXCEL_PATH")


###############GOOGLESHEET###################
scope = ['https://www.googleapis.com/auth/spreadsheets',
         'https://www.googleapis.com/auth/drive']

# Create an instance for the credentials
creds = Credentials.from_service_account_file('credentials.json', scopes=scope)

# Make authorization with gspread
try:
    client = gspread.authorize(creds)
except:
    print("Failed to authorize")

# Open the Google Sheet by id
sheet = client.open_by_key("1akV-G8UH8gVQoSD4n-5NBwddWM2-s-fF1lkxOH66R3M")
sheet1 = sheet.get_worksheet(0)
print("Openeed ggSsheet")
value_list = test_openxl.getvalue(path)
print("Getvalue Success")

def now_and_next_available_row(worksheet):
    str_list = list(filter(None, worksheet.col_values(1)))
    #print(str_list)
    return len(str_list),len(str_list)+1

lastrow,nextrow = now_and_next_available_row(sheet1)

from datetime import datetime
now = datetime.now()
dt_string = now.strftime("%Y%m%d %H%M")



if sheet1.cell(lastrow,1) != dt_string:
    sheet1.update_cell(nextrow,1, dt_string)
    for i in range(2,len(value_list)+2):
     sheet1.update_cell(nextrow,i,value_list[i-2])
    print('insertSucessfully')
    
else: 
    print("Already Inserted")

time.sleep(10)