import pandas as pd
import gspread
from google.oauth2.service_account import Credentials
import test_openxl
import os
import time
import sys
import logging
from dotenv import load_dotenv
from datetime import datetime
'''
THIS MODULE INTERACT WITH GOOGLESHEET
'''
load_dotenv(override=True)
logging.basicConfig(level=logging.INFO, filename='./logging/ggsheet_insert.log',format='%(asctime)s :: %(levelname)s :: %(message)s')

credentials_path = os.environ.get("CREDENTIALS_PATH")
sheet_id = os.environ.get("SHEET_ID")

###############GOOGLESHEET###################
scope = ['https://www.googleapis.com/auth/spreadsheets',
         'https://www.googleapis.com/auth/drive']

# Create an instance for the credentials
try:
    creds = Credentials.from_service_account_file(credentials_path, scopes=scope)
except FileNotFoundError:
    print("Cannot find credentials.json")
    logging.error("Cannot find credentials.json")
    sys.exit()
    


# Make authorization with gspread
try:
    client = gspread.authorize(creds)
except:
    print("Failed to authorize")
    logging.error("Failed to authorize")
    sys.exit()


def get_data():
    try: 
        value_list = test_openxl.getvalue("K9","K23")
        last_day = test_openxl.get_last_day()
        
    except:
        print("Failed to authorize")
        logging.error("Failed to authorize")
        
    return value_list, last_day

#def now_and_next_available_row(worksheet):
#    str_list = list(filter(None, worksheet.col_values(1)))
#    #print(str_list)
#    return len(str_list),len(str_list)+1

#value_list,last_day = get_data()   


def excecute(value_list,lastday,now):
    try: 
        sheet = client.open_by_key(sheet_id)
        sheet1 = sheet.get_worksheet(0)
        print("Openeed ggSsheet")
    except:
        print("Cannot open googlesheet")
        logging.error("Cannot open google sheet")
        return Exception
    
    #now = now.strftime("%Y%m%d %H%M")
    print(lastday)
    print(datetime.strptime(sheet1.acell("J2").value,"%m/%d/%Y"))
    sheet_last_update = datetime.strptime(sheet1.acell("J2").value,"%m/%d/%Y")
    if sheet_last_update != lastday:
        for i in range(2,len(value_list)+2):
         sheet1.update_cell(i,8,value_list[i-2])
        sheet1.update_acell("J2", last_day.strftime("%m/%d/%Y"))
        print('insertSucessfully')
    else:
        logging.info("Data already up to date")
        print("Data already up to date")

    sheet1.update_acell("K2",now.strftime("%m/%d/%Y %H:%M" ))



if __name__ == "__main__":
    value_list,last_day = get_data()
    now = datetime.now()

    excecute(value_list,last_day,now)
    time.sleep(3)