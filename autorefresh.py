
import os
import time
import logging
from dotenv import load_dotenv
import pandas as pd
import pyautogui
from datetime import datetime
import sys
import xlwings as xw

load_dotenv(override=True)
path = os.environ.get("REFRESH_EXCEL_PATH")
log_path = os.environ.get("LOG_AUTOREFRESH_PATH")
dimdate_path = os.environ.get("DIM_DATE_PATH")
close_image = os.environ.get("CLOSE_IMAGE")
refresh_image = os.environ.get("REFRESH_IMAGE")

logging.basicConfig(level=logging.INFO, filename=log_path,format='%(asctime)s :: %(levelname)s :: %(message)s')


logging.info(" autorefresh.exe started ")
print("autorefresh.exe started.")


def is_trading_day(day):
 today_date = pd.Timestamp(day.date())
 try:
  trading_df = pd.read_csv(dimdate_path, parse_dates=["date"],usecols=["date"])
 except:
  logging.error("Cannot find dimdate_normalday.csv")
  print("Cannot find dimdate_normalday.csv")
  raise Exception 
 return (trading_df["date"] == today_date).any()

def close_exists_excel():
 global path
 try:
    book = xw.Book(path)
    book.close()
 except Exception as e:
    print("No opened Exccel")

def excecute():
  global path

  pyautogui.scroll(100000)
  #print("Scrollled 1st time")

  ####  FIND CLOSE BUTTON #####
  for i in range(20):
   try:
    close_location  = pyautogui.locateOnScreen(close_image,confidence=0.8)
    if close_location:
     #print("Founded close button")
     pyautogui.moveTo(close_location,duration=0.1)
     #print("Moved to Close Button")
     logging.info("Closed the Activate Window")
     time.sleep(0.5)
     pyautogui.click()
     break
   except Exception:
    #print("Cannot find close button")
    logging.info("Can't find the Close actiavate window")
    time.sleep(2)
  
  pyautogui.scroll(200000)
  #print("Scrollled 2 time")

  #### FIND REFRESH BUTTON ####
  location =''
  for i in range(20):
   try:
    location = pyautogui.locateCenterOnScreen(refresh_image,confidence=0.4)
    if location:
     break
   except Exception:
    #print("Cannot find Refresh Button")
    logging.info("Can't find the Refresh Button")
    time.sleep(2)

  if location:
   logging.info("Found Refresh button")
   pyautogui.moveTo(location,duration=0.01)
   
   pyautogui.click()
   
   pyautogui.typewrite(str(datetime.now()))
   time.sleep(3)
   print("Refreshed")
   logging.info("Refeshed Successfully")
  else:
   print("cannot find Button")
   logging.error("Cannot Refresh")

def close():
 pyautogui.hotkey('ctrl','s')
 time.sleep(3)
 pyautogui.hotkey("alt","f4")
 logging.info("Save Successfully")
 print("Saved and Exited")
 time.sleep(3)

if __name__ == "__main__":
  today = datetime.now()
  close_exists_excel()
  time.sleep(1)
  if True:
  #if is_trading_day(today):
    try:
      
      os.system(f"start /max EXCEL.EXE {path}") #Open full size
      time.sleep(5)
      excecute()
      close()

    except:
     logging.error(f"Cannot open file {path}")
     print(f"Không thể mở file {path}, kiểm tra lại đường dẫn")
     sys.exit()
  else:
   logging.info("Not Trading Day")
   print(f"Not Trading Day, Exiting...")
   time.sleep(2)
 
