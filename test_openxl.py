from openpyxl import load_workbook
import os
import pandas
from datetime import datetime
from dotenv import load_dotenv
import os
load_dotenv(override=True)

'''
THIS MODULE INTERACTS WITH LOCAL EXCEL FILE
'''

path = os.environ.get("REFRESH_EXCEL_PATH")
workbook = load_workbook(path)
sheet = workbook.active



def getvalue(start,end):
 list = []
 #print(sheet["M5:M10"])
 for onesheet in sheet[f"{start}:{end}"]:
  list.append(onesheet[0].value)
 return list

def get_last_day():
 #print(sheet["D"][len(sheet['D'])-1].value)
 return sheet["D"][len(sheet['D'])-1].value

if __name__ == '__main__':
 print(path)
 list = getvalue(start="K9",end="K23")
 print(list)
 print(get_last_day())