# BLOOM - autorefresh .exe

# I. Overview

Trên đây là phần script Python cho tool Auto Refresh được triển khai trên máy Bloom 

Tool gồm task:

- [A](http://Autorefresh.py)uto Refresh          :  Tự động Refresh file excel dưới máy Bloom để lấy dữ liệu TVĐT mới nhất
- Google Sheet sync : Tự động lấy một vùng dữ liệu trên file excel TVĐT và sync lên google sheet

Các file exe và các file config được lưu trong folder **/dist .**Khi setup chỉ cần copy folder /dist và điều chỉnh các file config (chi tiết trong phần setup)

## 1. Autorefresh

Mỗi lần chạy, kiểm tra xem giao dịch không (lấy trong file dimdate_normalday.csv) 

Nếu đúng, bot bật file ⇒ điều khiển chuột đến ô Refresh màu vàng ⇒ Click và nhấn Enter ⇒  Save và Exit

### Dependencies:

- [autorefresh.py](http://autorefresh.py) : Scripts chạy task
- dimdate_normalday.csv: File csv chứa thông tin ngày giao dịch
- logging/autorefresh.log: File chứa log của Task
- close.png: Hình ảnh nút Close để bot nhận dạng ( Không cần thiết dưới máy Bloom)
- clicktest.png: Hình ảnh nút Refresh trên máy Bloom
- .env:
    - REFRESH_EXCEL_PATH: Đường dẫn đến file excel
    - LOG_AUTOREFRESH_PATH: Đường đẫn đến file log
    - DIM_DATE_PATH: Đường dẫn đến file csv ngày giao dịch (dimdate_normalday.csv)
    - CLOSE_IMAGE: Đường dẫn đến close.png
    - REFRESH_IMAGE: Đường dẫn đến clicktest.png

## 2. Google Sheet sync

Mỗi khi chạy, mở file excel và lấy data và lấy ngày cuối cùng có data ( test_openxl.py) 

Kết nối tới Google Sheet , Kiểm tra xem ngày cuối cùng có data của GoogleSheet (data_on_bloom) có giống ngày cuối cùng có data trên Bloom hay không. Nếu giống nhau, không cần update data, nếu không thì update data vừa lấy được và sửa lại data_on_bloom

Cuối cùng, thay đổi last_update trên google Sheet bằng thời gian task được chạy

### Dependencies

- ggsheet_insert.py: Scripts chạy task (Thao tác với GoogleSheet)
- test_openxl.py: Scripts thao tác với (File Excel ở Bloom)
- credentials.json: File json để xác thực account với google sheet (Ref [Create and delete service account keys  |  IAM Documentation  |  Google Cloud](https://cloud.google.com/iam/docs/keys-create-delete))
- logging/googlesheet_insert.log: File chứa log của Task
- .env
    - LOG_GOOGLESHEET_INSERT_PATH: Đường dẫn tới file log
    - CREDENTIALS_PATH: Đường dẫn tới file credential.json
    - SHEET_ID: ID của File GoogleSheet ( Lấy từ url của File)

# II. Setup

### 1. Build exe file

Open terminal, cd to your directory

Run this to build autorefresh.exe file from autorefresh.py file

```python
pyinstaller --onefile autorefresh.py
```

Run this to build googlesheet_insert.exe file from googlesheet_insert.py file

```python
pyinstaller --onefile googlesheet_insert.py
```

Sau khi chạy thành công, file excel được lưu trong thư mục dist (nếu đã tồn tại file exe trước đó, tự động replace)

### 2. Setup exe file

Sau khi build, chỉ cần quan tâm đến folder dist

Lúc này kiểm tra xem folder dist có đủ các thành phần sau không: 

- /logging
- .env
- clicktest.png
- close.png
- credentials.json
- dimdate_normalday.csv
- autorefresh.exe
- ggsheet_insert.exe

Nếu không đủ, tự động thêm vào folder /dist từ repo source code

**Config:**

Mở file .env, thay đổi các  biến cho giống mô tả ở phần Overview

### 3. Setup Schedule job

< Đang kiểm tra, Sẽ cập nhật sau>

#